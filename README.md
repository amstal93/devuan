Devuan Docker Base Images
=========================

For reasons outlined in the bottom part of a [blog post][1] of mine, I
decided to roll my own Devuan Docker base images.  This project is the
result of that endeavour.  Its repository contains all the code used
to build several Devuan [image variants](#image-variants) for a number
of [supported versions](#supported-versions).

TL;DR
-----

You can pull the latest Devuan base image with

``` sh
docker pull registry.gitlab.com/paddy-hack/devuan
```

A full list of image variants and supported versions is available via
the [container registry][2].

Image Variants
--------------

The [registry][2] contains the following image variants:

- *devuan* contains the result of a minimal ``debootstrap``, minus any
  packages that were deemed not essential for Devuan.
- *devuan/slim* is a *devuan* sibling that's been trimmed of "flab".
  It comes without any of the usual documentation bits (but keeps the
  `copyright` information!).  It assumes that a POSIX/C locale is all
  you'll ever need in terms of program feedback as well.  Package
  recommendations are pertinently ignored (and actively targeted for
  auto-removal).
- *devuan/builder* derives from the *devuan/slim* image and adds the
  "standard" packages needed to build binary packages.  It achieves
  this by installing `build-essential`.

There is also a *devuan/migrated* image, used during the build of the
above for now (see #5 and [below](#how-did-i-get-there)), but that is
only made available for reference, reproducability and accountability
purposes.

Supported Versions
------------------

The intent is to provide versions for all of [upstream][3]'s
published [releases][4] that are supported by `debootstrap`.

- *latest* points to the current Devuan release.  As of writing that
  is *beowulf*.  The *latest* version is a Docker convention and this
  project thinks it should point to something tried, tested and rock
  solid rather than to something bleeding edge that might blow up in
  your face any time.
- *jessie*, the first Devuan release, tried, tested, rock solid and
  aging gracefully now that it has been superseded by
- *ascii*, the second Devuan release, tried, tested and rock solid and
  superseded by
- *beowulf*, the current Devuan release, tried, tested, rock solid.
- *chimaera*, what will soon(?) become the current Devuan release, but
  not before it's deemed ready.  Think of this as "brick solid".
- *ceres*, where all the "Works for me (famous last quote)", bleeding
  edge kind of thingies enter the heading-for-release cycle.  Trying
  to build anything on top of this is prone to sucking you in.  Think
  of this as "quicksand solid" :stuck_out_tongue_winking_eye:

The above release or code name based versions point to most recent,
successfull build of each image variant.  This means that with every
new (scheduled) build, these versions point to a **different** image.
If that kind of fluidity or "instability" unnerves you, you may want
to use one of the available date-stamped versions instead.  These
date-stamped versions have a *-YYYY-MM-DD* tagged onto their release
or code name, e.g. *ascii-2017-11-03*.

Please note that *stable*, *testing* and *unstable* versions are
purposely **not** available to avoid confusion with their Debian
counterparts.  The Debian notions of *stable* and the like, do not
necessarily correspond to what the [Devuan Project][3] considers
*stable* and the like at any given time.

Pulling Images
--------------

Given the above, how would you pull the image you need?  Let's say you
have a need for the (most recently successfully built) *ascii* version
of *devuan/slim*.  All you need to do is

``` sh
docker pull registry.gitlab.com/paddy-hack/devuan/slim:ascii
```

Let's say you live life somewhat dangerously and need a 2017-11-03
dated *ceres* version of the *devuan/builder* variant.  Just run

``` sh
docker pull registry.gitlab.com/paddy-hack/devuan/builder:ceres-2017-11-03
```

and you should be ready to go (assuming there is such a dated version
to begin with, of course).

In general, you would determine the `$variant` and the `$version` you
need and then simply

``` sh
docker pull registry.gitlab.com/paddy-hack/$variant:$version
```

How did I get there?
====================

I had to start somewhere, so the first step takes a Debian base image
and converts that following the Devuan [migration instructions][5].
The image is pushed to the registry, mainly for reference purposes,
and used to run Devuan's ``debootstrap``.  This produces a ``rootfs``
that is imported to the Docker cache, pushed, re-tagged and pushed
again to the [container registry][4].

BTW, you can pull the migrated image with

``` sh
docker pull registry.gitlab.com/paddy-hack/devuan/migrated
```

if so inclined.

 [1]: https://paddy-hack.gitlab.io/posts/sandwiching-docker-with-devuan/
 [2]: https://gitlab.com/paddy-hack/devuan/container_registry
 [3]: https://devuan.org/
 [4]: https://devuan.org/os/releases
 [5]: https://devuan.org/os/documentation/dev1fanboy/Upgrade-to-Devuan
