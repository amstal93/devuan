#!/bin/sh -eu
# bootstrap.sh -- a Devuan base image
# Copyright (C) 2017, 2018  Olaf Meeuwissen
#
# License: GPL-3.0+

DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

apt-get update
apt-get install debootstrap -q -y

suite=$1
rootfs=$2-$suite

test -e /usr/share/debootstrap/scripts/$suite \
    || ln -s ceres /usr/share/debootstrap/scripts/$suite

debootstrap \
    --force-check-gpg \
    --no-merged-usr \
    --variant=minbase $suite $rootfs http://deb.devuan.org/merged

mount -t devpts devpts $rootfs/dev/pts

chroot $rootfs apt-mark auto '.*' \
       | sed '/not installed/d'
chroot $rootfs apt-mark manual devuan-keyring

bindir=usr/local/bin
install -m 0755 -o root -g root scripts/* $rootfs/$bindir

for script in $(ls $rootfs/$bindir); do
    echo "+ /$bindir/$script"
    chroot $rootfs /$bindir/$script $suite
done

chroot $rootfs apt-get --purge autoremove -q -y
chroot $rootfs /$bindir/docker-apt-clean $suite

umount $rootfs/dev/pts

tar -caf $rootfs.tar.gz \
    --directory $rootfs \
    --exclude './dev/**' \
    --numeric-owner \
    --transform 's,^\./,,' .
